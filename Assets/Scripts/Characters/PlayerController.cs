﻿using UnityEngine;

namespace Characters {
    public class PlayerController : MonoBehaviour {
        private CreatureController creatureController;
    
        private void Start() {
            creatureController = GetComponent<CreatureController>();
        }

        private void FixedUpdate() {
            creatureController.SetHInput(Input.GetAxisRaw("Horizontal"));
        
            if (Input.GetAxis("Jump") > 0) {
                creatureController.Jump();
            }
        }
    }
}