﻿using System;

namespace Triggers {
    
    [Serializable]
    public struct TriggerAction {
        public TriggerActionType type;
        public float duration;
    }
}