﻿using System.Collections.Generic;
using UnityEngine;

namespace Triggers {
    public class Trigger : MonoBehaviour {
        public List<TriggerAction> actions;
    }
}