﻿namespace Triggers {
    public enum TriggerActionType {
        Nothing,
        Jump,
        Stay,
        WalkNext,
        WalkBack
    }
}