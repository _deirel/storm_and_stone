﻿using UnityEngine;

namespace Characters {
    public class CreatureController : MonoBehaviour {
        private const float E = 0.01f;

        private Rigidbody2D body;
        private Animator animator;

        public float maxSpeed = 7f;
        public float jumpSpeed = 12f;

        private const float moveForce = 100f;
        private const float groundDrag = 0.5f;
        private const float airDrag = 0.97f;

        private bool facingLeft = true;

        private float hInput;
        private float hInputAbs;
        private bool makeJump;

        private RaycastHit2D[] hitsBuffer = new RaycastHit2D[5];
        private bool isGrounded;

        void Start() {
            body = GetComponent<Rigidbody2D>();
            animator = GetComponent<Animator>();
        }

        public void SetHInput(float value) {
            hInput = value;
            hInputAbs = Mathf.Abs(hInput);
        }

        // TODO Если !isGrounded, можно попробовать "задание" на прыжок с таймаутом
        public void Jump() {
            makeJump = true;
        }

        public void WalkLeft() {
            SetHInput(-1);
        }

        public void WalkRight() {
            SetHInput(1);
        }

        public void WalkBack() {
            if (facingLeft) WalkRight();
            else WalkLeft();
        }

        public void WalkNext() {
            if (facingLeft) WalkLeft();
            else WalkRight();
        }

        public void Stay() {
            SetHInput(0);
        }

        private void FixedUpdate() {
            checkIsGrounded();

            var velocity = body.velocity;

            if (hInputAbs > 0) {
                body.AddForce(new Vector2(hInput * moveForce, 0));
            }

            if (hInputAbs < E) {
                velocity.x *= isGrounded ? groundDrag : airDrag;
            }

            if (Mathf.Abs(velocity.x) < E) {
                velocity.x = 0;
            }
            else {
                velocity.x = Mathf.Clamp(velocity.x, -maxSpeed, maxSpeed);
            }

            if (isGrounded && makeJump) {
                velocity.y = jumpSpeed;
            }

            makeJump = false;

            body.velocity = velocity;

            animator.SetFloat("speed", Mathf.Abs(body.velocity.x));

            if (hInput < 0 && !facingLeft || hInput > 0 && facingLeft)
                flipImage();
        }

        private void checkIsGrounded() {
            var numHits = body.Cast(Vector2.down, hitsBuffer, 0.05f);
            if (numHits == 0) {
                isGrounded = false;
                animator.SetBool("grounded", false);
            }
            else {
                for (var i = 0; i < numHits; i++) {
                    var hit = hitsBuffer[i];
                    if (hit.normal.y > 0.8f) {
                        isGrounded = true;
                        animator.SetBool("grounded", true);
                        return;
                    }
                }
            }
        }

        private void flipImage() {
            facingLeft = !facingLeft;
            var scale = body.transform.localScale;
            scale.x = -scale.x;
            body.transform.localScale = scale;
        }
    }
}