﻿using System.Linq;
using Characters;
using UnityEngine;

namespace Portal {
    public class Portal : MonoBehaviour {
        public string characterId;
        public PortalDirection direction;

        private GameObject prefab;

        private void Start() {
            prefab = Resources.Load<GameObject>("Characters/Girl");
        }

        public void Spawn() {
            var girl = Instantiate(prefab);
            girl.transform.position = transform.position + transform.up * 0.5f;

            girl.AddComponent<AIController>();

            var controller = girl.GetComponent<CreatureController>();
            switch (direction) {
                case PortalDirection.Left:
                    controller.WalkLeft();
                    break;
                
                case PortalDirection.Right:
                    controller.WalkRight();
                    break;
            }

//            var angle = Random.Range(0, Mathf.PI);
//            var velocity = new Vector2(Mathf.Cos(angle), Mathf.Sin(angle)) * 50f;
//            girl.GetComponent<Rigidbody2D>().velocity = velocity;
        }

        private string Capitalize(string str) {
            return char.ToUpper(str.First()) + str.Substring(1);
        }
    }

    public enum PortalDirection {
        Left,
        Right,
        Stay
    }
}