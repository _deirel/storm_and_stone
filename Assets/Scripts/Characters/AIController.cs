﻿using System.Collections;
using Triggers;
using UnityEngine;

namespace Characters {
    public class AIController : MonoBehaviour {
        private CreatureController creatureController;
        private Trigger currentTrigger;

        private void Start() {
            creatureController = GetComponent<CreatureController>();
        }

        private void OnTriggerEnter2D(Collider2D other) {
            if (!other.CompareTag("Trigger"))
                return;

            var trigger = other.GetComponent<Trigger>();
            if (trigger == currentTrigger)
                return;

            if (currentTrigger != null) {
                StopCoroutine(RunActionsCoroutine());
            }

            currentTrigger = trigger;
            StartCoroutine(RunActionsCoroutine());
        }

        private IEnumerator RunActionsCoroutine() {
            foreach (var action in currentTrigger.actions) {
                switch (action.type) {
                    case TriggerActionType.Jump:
                        creatureController.Jump();
                        break;

                    case TriggerActionType.Stay:
                        creatureController.Stay();
                        break;

                    case TriggerActionType.WalkBack:
                        creatureController.WalkBack();
                        break;

                    case TriggerActionType.WalkNext:
                        creatureController.WalkNext();
                        break;
                }

                if (Mathf.Approximately(action.duration, 0f)) {
                    yield return null;
                }
                else {
                    yield return new WaitForSeconds(action.duration);
                }
            }

            currentTrigger = null;
        }
    }
}