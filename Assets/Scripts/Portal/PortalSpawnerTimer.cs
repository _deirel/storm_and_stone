﻿using System.Collections;
using UnityEngine;

namespace Portal {
    public class PortalSpawnerTimer : MonoBehaviour {
        public float timeout = 1f;

        private Portal portal;

        private void Start() {
            portal = GetComponent<Portal>();
            StartCoroutine(Spawn());
        }

        private IEnumerator Spawn() {
            for (;;) {
                yield return new WaitForSeconds(timeout);
                portal.Spawn();
            }
        }
    }
}