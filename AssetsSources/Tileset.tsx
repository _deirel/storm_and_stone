<?xml version="1.0" encoding="UTF-8"?>
<tileset name="Tileset" tilewidth="50" tileheight="50" tilecount="136" columns="17">
 <image source="til_m.png" width="850" height="400"/>
 <tile id="46">
  <objectgroup draworder="index">
   <object id="1" x="22" y="0" width="28" height="23"/>
  </objectgroup>
 </tile>
 <tile id="47">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="50" height="23"/>
  </objectgroup>
 </tile>
 <tile id="48">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="28" height="23"/>
  </objectgroup>
 </tile>
 <tile id="62">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="50" height="50"/>
  </objectgroup>
 </tile>
 <tile id="63">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="50" height="50"/>
  </objectgroup>
 </tile>
 <tile id="64">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="38" height="50"/>
  </objectgroup>
 </tile>
 <tile id="78">
  <objectgroup draworder="index">
   <object id="1" x="10" y="0" width="40" height="50"/>
  </objectgroup>
 </tile>
 <tile id="79">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="50" height="50"/>
  </objectgroup>
 </tile>
 <tile id="80">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="50" height="50"/>
  </objectgroup>
 </tile>
 <tile id="81">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="50" height="50"/>
  </objectgroup>
 </tile>
 <tile id="82">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="50" height="50"/>
  </objectgroup>
 </tile>
 <tile id="83">
  <objectgroup draworder="index">
   <object id="2" x="0" y="0" width="50" height="50"/>
  </objectgroup>
 </tile>
 <tile id="84">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="50" height="50"/>
  </objectgroup>
 </tile>
</tileset>
