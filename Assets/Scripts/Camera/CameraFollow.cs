﻿using UnityEngine;

namespace Camera {
    public class CameraFollow : MonoBehaviour {
        public GameObject target;

        private void Update() {
            UpdatePosition();
        }

        private void UpdatePosition() {
            var position = target.transform.position;
            position.z = transform.position.z;
            transform.position = position;
        }
    }
}